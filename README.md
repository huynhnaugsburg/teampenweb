Web Address Book Version1

In this project a web page will be functioning as a small address book, it will display 
and allow editing of data about people. The page is driven by javascript functions on the 
operating part of the web page. Html and css is used for the page layout. A display 
containing a list of names will appear on the left side of the page, and when a name is 
selected, display the details for that contact will appear on the right side of the page. 
The details for a person include a adress field, phone number field, and an email address field.
----------------------
Instructions:
To add new a new contact:
Push the button the the upper left of the page and a new user will be made that will be in edit mode.
NOTE: The save button must be pressed before choosing another contact or the new contact will not be saved.

To edit contact info:
Hit the edit button on the upper right of the page after a contact has been chosen.
A user can change information already there by editing the text boxes or by putting new inforation into the boxes.
The add button must be pressed if a user wishes to add new info to the contact.
The delete button can be used to delete a line of information from the contact.
NOTE: All of the changes are not permanent until the save button is pressed. So if a user clicks on a new contact before
it was saved then all edited information will be lost.

To delete a conact:
A user can hit the delete button next to the save button after they have entered edit mode.
NOTE: Delete is permanent and will remove the contact from the list also if a user decides to delete all of the contacts
a place holder will be made as a contact. 
----------------------
Built With:
Html - Structuring the webpage.
CSS - Styling the webpage
javascript - functionality

----------------------
Author(s):
Nghia Huynh
Erik Ayavaca-Tirado
Patrick Deng

----------------------
License:
This project is license under nobody and is free to use for all.

----------------------
Acknowledgements:
Google - Telling me what to do.