/*
PEN.js
Nghia Huynh
Erik Ayavaca-Tirado
Patrick Deng
This file is used to format and change the style of our sections of html.
*/
//These are hard coded to be used by the HTML page to fill it with info.
var names = ["Nghia Apples","Eric Oranges","Patrick Moops"];
var addresses = [["1506 Oof Ave", "567 Goosepickle Lane"],["9092 Cruds Street"], ["78 Meow BVL","543 MoopMeep Rd"]];
var phones = [["7617210009"],["8769061234","1234281238"],["7896785432", "9870091224"]];
var emails = [["apples@apples.com"],["bananana@banana.com","pickles@pickles.org"],["oranges@oranges.com"]];
/*
The function below is used to load the information from the arrays to the addContact funtion. 
It is ran immediatedly as the body of the document is loaded.
*/
function onLoad()
{   
    for (var i = 0; i < names.length; i++) 
    { 
        addContact(names[i], names, addresses, phones, emails);
    }
}
/*
This adds a new cell to the table in the first div and sets an onclick function to the cell.
*/
function addContact(newname)
{
    var table = document.getElementById("myTable");
    var row = table.insertRow(table.length);
    var name = row.insertCell(0);
    name.innerHTML = newname;
    row.setAttribute("onclick","loadInfo(this, names, addresses, phones, emails)");
}
/*
This function is for when a user clicks on the name of a contact.
It uses the information of the cell location to generate a page on the div 
on the right side.
*/
function loadInfo(cell, names, addresses, phones, emails)
{
    //This part removes an element from the contacts table if it is tagged with a specific ID.
    //This is so that if a user tries to click on another contact while the are in edit mode it removes the edit contact.
    var newcontactcheck = document.getElementById("newcontact");
    if (newcontactcheck != null)
    {
        newcontactcheck.remove();
    }
    // This part of the code gives an id to the selected cell so that its color is changed.
    var select = document.getElementById("chosen");
    // If is used to see if a cell has that id if so remove the id and assign it to current cell.
    if (select == null)
    {
        cell.setAttribute("id","chosen");
    }
    else
    {
        select.removeAttribute("id");
        cell.setAttribute("id","chosen");
    }

    var elementindex = cell.rowIndex - 1;                   //Index of the cell 
    var parentnode = document.getElementById("info");       //Node for the info div.

    // This code is to hide the delete button while it is in view mode.
    var deletebtn = document.getElementById("deletebtn"); 
    deletebtn.style.display = "none";

    //Reveals the edit button and assigns a function to it and a value based on the cell index.
    var editbtn = document.getElementById("editinfobtn");
    editbtn.style.display = "block";
    editbtn.setAttribute("onclick","editInfo(names, addresses, phones, emails)");
    editbtn.innerHTML = "Edit Info";
    editbtn.setAttribute("value",elementindex);

    // Loads name of the contact from array to page.
    var name = document.getElementById("nameofcontact");
    name.innerHTML = names[elementindex];

    //Takes information from array to call a table builder function
    // This then replaces the old table with the new
    // It is repeated 3 times for the 3 different sections.
    var oldaddress = document.getElementById("addresses");
    var newaddress = createDisplayTable((elementindex), addresses);
    newaddress.setAttribute("id","addresses");
    parentnode.replaceChild(newaddress, oldaddress);

    var oldphones = document.getElementById("phones");
    var newphones = createDisplayTable((elementindex), phones);
    newphones.setAttribute("id","phones");
    parentnode.replaceChild(newphones, oldphones);

    var oldemails = document.getElementById("emails");
    var newemails = createDisplayTable((elementindex), emails);
    newemails.setAttribute("id","emails");
    parentnode.replaceChild(newemails, oldemails);
}

// Takes in array and an index so that it can gather data from the sub array in each section.
// Returns new table to caller.
function createDisplayTable(index, array)
{
    //Creates new table
    newtable = document.createElement("table");
    //This for loops iterates over the sub array and fills the table cells with the data.
    for (var k = 0; k < array[index].length; k++) 
    { 
        row = newtable.insertRow(-1);
        textnode = row.insertCell(-1);

        textnode.innerHTML = array[index][k];
    }
    return newtable
}

//Creates a table similar to the table above, but instead of static values each cell holds a text input.
function createEditTable(index, item)
{
    //Creates new table
    newtable = document.createElement("table");
    for (var k = 0; k < item[index].length; k++) 
    { 
        //Adds a new row to the table with a new text box and delete button.
        row = newtable.insertRow(-1);
        textnode = row.insertCell(-1);
        deletebtn = row.insertCell(-1);
        //fills a text box with information from an array
        var txtbox = document.createElement("input");
        txtbox.setAttribute('type','text');
        txtbox.setAttribute('value',item[index][k]);
        //Appends text information to the txt node.
        textnode.innerHTML = "";
        textnode.appendChild(txtbox);
        //Appends the delete button to the table.
        newbtn = document.createElement("button");
        newbtn.innerHTML = "Delete";
        newbtn.setAttribute("onclick","deleteInfo(this)");
        deletebtn.appendChild(newbtn);
    }

    //This section functions similar to the delet rows, but has an add function.
    addrow = newtable.insertRow(-1);
    emptytxtbox = addrow.insertCell(-1);
    addbtn = addrow.insertCell(-1);

    var addtxtbox = document.createElement("input");
    addtxtbox.setAttribute('type','text');
    addtxtbox.setAttribute('value',"");

    newbtn = document.createElement("button");
    newbtn.innerHTML = "Add";
    newbtn.setAttribute("onclick","addInfo(this)");
    addbtn.appendChild(newbtn);

    emptytxtbox.appendChild(addtxtbox);
    addbtn.appendChild(newbtn);
    //Returns the table to the caller.
    return newtable
}
// This function is used to activate edit mode for the info section of the page.
function editInfo(names, addresses, phones, emails)
{
    //This reveals the delete button while in edit mode.
    var newcontact = document.getElementById("newcontact");
    if (newcontact == null)
    {
        var deletebtn = document.getElementById("deletebtn");
        deletebtn.style.display = "block";
    }
    //These temp arrays are used to create a temporary array that will no affect the actual arrrays until the user hits save.
    tempaddresses = JSON.parse(JSON.stringify(addresses));
    tempphones = JSON.parse(JSON.stringify(phones));
    tempemails = JSON.parse(JSON.stringify(emails));
    //Changes the edit button to a save button on the page.
    button = document.getElementById("editinfobtn");
    button.innerHTML = "Save";
    button.setAttribute("onclick","saveInfo()");
    var elementindex = button.value;
    var parentnode = document.getElementById("info");

    //Fills a the name space with a place holder if one is not given.
    var name = document.getElementById("nameofcontact");
    if (names[elementindex] != undefined)
    {
        var txtbox = document.createElement("input");
        txtbox.setAttribute('type','text');
        txtbox.setAttribute('value', names[elementindex]);
        name.innerHTML = "";
        name.appendChild(txtbox);
    }

    //These three sections change the info tables to editable versions with text boxes using the createEditTable functions.
    var oldaddress = document.getElementById("addresses");
    var newaddress = createEditTable(elementindex, addresses);
    newaddress.setAttribute("id","addresses");
    parentnode.replaceChild(newaddress, oldaddress);

    var oldphones = document.getElementById("phones");
    var newphones = createEditTable(elementindex, phones);
    newphones.setAttribute("id","phones");
    parentnode.replaceChild(newphones, oldphones);

    var oldemails = document.getElementById("emails");
    var newemails = createEditTable(elementindex, emails);
    newemails.setAttribute("id","emails");
    parentnode.replaceChild(newemails, oldemails);
}
//This is for when a user wants to add another row of information to a section.
function addInfo(button)
{
    //This line gets the index of the current row.
    var elementindex = document.getElementById("editinfobtn").value;
    //This part gets the information of the parent table of the button that was pressed.
    info = button.parentNode.parentNode.parentNode.parentNode;
    var newtxt = info.rows[info.rows.length - 1].cells[0].childNodes[0].value;
    //These ifn statment checks for what section is being changed and if there is text to be added.
    //It will call the editInfo function to rebuild the page.
    if (info.id == "addresses" && newtxt != "")
    {
        tempaddresses[elementindex].push(newtxt);
        editInfo(names, tempaddresses, tempphones, tempemails);
    }
    else if (info.id == "phones" && newtxt != "")
    {
        tempphones[elementindex].push(newtxt);
        editInfo(names, tempaddresses, tempphones, tempemails);
    }
    else if (info.id == "emails" && newtxt != "")
    {
        tempemails[elementindex].push(newtxt);
        editInfo(names, tempaddresses, tempphones, tempemails);
    }
}
//This button is for when a user want to delete a row from the info page.
function deleteInfo(button)
{
    //This part figures our where the row is located on the page.
    var elementindex = document.getElementById("editinfobtn").value;
    info = button.parentNode.parentNode.parentNode.parentNode;
    rownum = button.parentNode.parentNode.rowIndex;
    //This section checks for what section is having its row removed.
    if (info.id == "addresses")
    {
        tempaddresses[elementindex].splice(rownum,1);
    }
    else if (info.id == "phones")
    {
        tempphones[elementindex].splice(rownum,1);
    }
    else if (info.id == "emails")
    {
        tempemails[elementindex].splice(rownum,1);
    }
    //The function is called to rebuild the new page after the remove.
    editInfo(names, tempaddresses, tempphones, tempemails);
}
//This function is to make the real changes to the global array and save it so that the changes are permanent.
function saveInfo()
{
    //These hold the array for the new values that will be changed on the page.
    var elementindex = document.getElementById("editinfobtn").value;
    var newname = document.getElementById("nameofcontact").childNodes[0].value;
    var oldaddress = document.getElementById("addresses");
    var oldphones = document.getElementById("phones");
    var oldemails = document.getElementById("emails");
    var newaddress = [];
    var newphones = [];
    var newemails = [];
    //This checks if the current edit page is a new contact page and if it is creates a new contact based on the info.
    var newcontactcheck = document.getElementById("newcontact");
    if (newcontactcheck != null)
    {
        newcontactcheck.setAttribute("onclick","loadInfo(this, names, addresses, phones, emails)");
        newcontactcheck.removeAttribute("id");
    }
    //These for loops are used to load the current info from the edit page to arrays
    for (var i = 0; i < (oldaddress.rows.length - 1); i++)
    {
        if (oldaddress.rows[i].cells[0].childNodes[0].value != "")
        {
            newaddress.push(oldaddress.rows[i].cells[0].childNodes[0].value);
        }
    }
    for (var j = 0; j < (oldphones.rows.length - 1); j++)
    {
        if (oldphones.rows[j].cells[0].childNodes[0].value != "")
        {
            newphones.push(oldphones.rows[j].cells[0].childNodes[0].value);
        }
    }
    for (var k = 0; k < (oldemails.rows.length - 1); k++)
    {
        if (oldemails.rows[k].cells[0].childNodes[0].value != "")
        {
            newemails.push(oldemails.rows[k].cells[0].childNodes[0].value);
        }
    }
    //This saves the name of the person if they have changed it.
    names[elementindex] = newname;
    var table = document.getElementById("myTable");
    var temp = parseInt(elementindex) + 1;
    var newcell = table.rows[temp];
    newcell.childNodes[0].innerHTML = newname;
    //This puts the new saved info into the global arrays.
    addresses[elementindex] = newaddress;
    phones[elementindex] = newphones;
    emails[elementindex] = newemails;
    //Rebuilds the regular info page after the code is ran.
    loadInfo(newcell, names, addresses, phones, emails);
}
//This function is used when a user want to add a new contact to the page.
function newContact(names, addresses, phones, emails)
{
    //This if statement checks if the user is already edititing a new contact and will not let them make a new one until they finish.
    var newcontactcheck = document.getElementById("newcontact");
    if (newcontactcheck == null)
    {
        //This code is holding the new temp info for the new contact until the save button is pressed.
        tempnames = JSON.parse(JSON.stringify(names));
        tempaddresses = JSON.parse(JSON.stringify(addresses));
        tempphones = JSON.parse(JSON.stringify(phones));
        tempemails = JSON.parse(JSON.stringify(emails));
        tempaddresses.push(["Address"]);
        tempphones.push(["Phone"]);
        tempemails.push(["Email"]);
        tempnames.push("Name");
        //Adds a Name place holder for the left side of the page.
        var table = document.getElementById("myTable");
        var elementindex = table.rows.length - 1;
        var row = table.insertRow(table.length);
        var name = row.insertCell(0);
        name.innerHTML = "Name";
        //Sets id of the new contact so that the page can make sure that the user must save.
        row.setAttribute("id","newcontact")
        //This reveals the edit button and gives it a index value based on the current table row.
        var btn = document.getElementById("editinfobtn");
        btn.style.display = "block";
        btn.setAttribute("value",elementindex);
        //Calls edit mode for the new contact.
        editInfo(tempnames, tempaddresses, tempphones, tempemails);
        //Hides delete button because the user does not need it if they dont save.
        var deletebtn = document.getElementById("deletebtn");
        deletebtn.style.display = "none";
    }
}
//This function deletes a contact from the page when in edit mode.
function deleteContact(names, addresses, phones, emails)
{
    //This part removes the contact from the left side of the page
    var table = document.getElementById("myTable").childNodes[1].childNodes;
    var select = document.getElementById("chosen");
    var elementindex = document.getElementById("editinfobtn").value;
    select.remove();
    //This removes the information of the contact from the global arrays.
    names.splice(elementindex, 1);
    addresses.splice(elementindex,1);
    phones.splice(elementindex,1);
    emails.splice(elementindex,1);
    //This code runs if the user deletes all of the users from the page, so an empty contact is made as a place holder.
    if (table.length < 3)
    {
        newContact(names, addresses, phones, emails);
        saveInfo();
    }
    //This is the default load info after a contact has been deleted.
    loadInfo(table[2], names, addresses, phones, emails);
}